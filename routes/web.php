<?php


Route::get('/', function () {
    return view('Home.index');
})->name('home');

Route::get('/Register', function () {
    return view('Account.Register');
})->name('register');

Route::get('/profile', function () {
    return view('Home.profile');
})->name('profile');