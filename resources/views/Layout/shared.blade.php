<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="{{ asset('imgs/favIcon.png') }}" type="image/x-icon" />
        <title>Fashion</title>
        <!--  bootstrap4 framework css  -->
        <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
        <!-- animate css -->
        <link href="{{ asset('css/animate.min.css') }}" rel="stylesheet">
        <!-- Icons css -->
        <link href="{{ asset('css/Fonts.css') }}" rel="stylesheet">
        <!-- my Style css -->
        <link href="{{ asset('css/main.css') }}" rel="stylesheet">
        <!-- Jquery  -->
        <script src="{{ asset('js/jquery.js') }}"></script>
        <!-- popper js  -->
        <script src="{{ asset('js/popper.js') }}"></script>
        <!-- bootstrap 4 js   -->
        <script src="{{ asset('js/bootstrap.min.js') }}"></script>
        @yield('style')
    </head>

     @yield('body')
</html>
