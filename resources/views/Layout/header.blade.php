@extends('layout.shared')
@section('body')
    <body>
    {{-- Navigationbar --}}
    <nav class="navbar  pt-5 d-flex justify-content-around" id="navbar" style="z-index: 1">
        <div class="d-flex">
            <button class="btn" onclick="openNav()" id="menu-Btn"><i class="icon-menu"></i></button>
            <a class="navbar-brand align-self-center text-black" href="{{ route('home') }}">FASHION. CLCT</a>
        </div>
        <ul class="list-inline mb-0 navbar-menu ">
            <li class="list-inline-item active">

                <a class="nav-link text-black" href="#">Women CATALOGUE</a>
            </li>
            <li class="list-inline-item">

                <a class="nav-link text-black" href="#">SORT BY FOLLOWERS<i class="icon-arrow-down text-muted mx-1"></i></a>
            </li>
        </ul>
        <ul class="mb-0 navbar-menu">
            <a href="{{ route('register') }}" class="text-black d-flex ">Register
                <i class="icon-arrow-right d-flex align-self-center mx-1"></i>
            </a>
        </ul>

    </nav>

    <!--  side menu -->
    <div id="myNav" class="overlay">
        <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>

        <div class="overlay-content">
            <a href="#">About</a>
            <a href="#">Blog</a>
            <a href="#">Community</a>
            <a href="#">Contact</a>
            <a href="#" class="side-menu-item">Men CATALOGUE</a>
            <a class="side-menu-item " href="{{ route('register') }}">Register</a>
        </div>
    </div>

    <div class="container-fluid" style="min-height: 92vh">
        @yield('content')
    </div>

    <script>
        function openNav() {
            document.getElementById("myNav").style.width = "100%";
        }

        function closeNav() {
            document.getElementById("myNav").style.width = "0%";
        }
    </script>
    </body>
@endsection