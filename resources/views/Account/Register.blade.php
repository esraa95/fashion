@extends('layout.shared')
@section('style')
        <!-- register css -->
<link href="{{ asset('css/register.css') }}" rel="stylesheet">
    @endsection
@section('body')
<body>
    <div class="container-fluid min-h-100vh">
        <div class="row d-flex h-100" style="min-height: inherit;">
            <div class="col-md-12 col-lg-6 col-sm-12 col-12 animated zoomIn slow" id="coverBlk">

            </div>
            <div class="col-md-12 col-lg-6 col-sm-12 col-12 justify-content-center align-self-center">
                <div class="col-md-8 offset-md-2 offset-lg-0 col-lg-12 col-xl-8 offset-xl-2">
                    <h1 class="text-center animated bounce"><a class="text-black" href="{{ route('home') }}">FASHION. CLCT</a></h1>
                    <h5 class="text-center animated bounceIn text-muted ff-cursive">Fashion Social Network Tool</h5>
                    <div class="container text-center my-4">
                        <h5 class="mb-3 animated tada ">SignUp with</h5>
                        <div class="animated bounceIn">
                            <label class="mx-2 px-2  Icon-border hoverAni"><i class="icon-google-plus"></i></label>
                            <label class="mx-2 px-3 py-2 Icon-border hoverAni"><i class="icon-instagram"></i></label>
                            <label class="mx-2 px-2  Icon-border hoverAni"><i class="icon-facebook"></i></label>
                        </div>
                    </div>
                    <div class="row mb-4 bounceInLeft animated ">
                        <div class="col-sm-5 col-5 col-md-5 pr-0">
                            <hr>
                        </div>
                        <span class="col-md-2 col-sm-2 col-2 text-center">OR</span>
                        <div class="col-sm-5 col-5 col-md-5 pr-0">
                            <hr>
                        </div>
                    </div>
                <form class="needs-validation zoomIn animated" novalidate="">
                    <div class="form-row">
                        <div class="col-sm-6 col-md-6 mb-3">
                            <label for="firstName">First name</label>
                            <input type="text" class="form-control" id="firstName" placeholder="First name"  required="" autofocus autocomplete="off">
                            <div class="valid-feedback">
                                Looks good!
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-6 mb-3">
                            <label for="lastName">Last name</label>
                            <input type="text" class="form-control" id="lastName" placeholder="Last name"  required="" autocomplete="off">
                            <div class="valid-feedback">
                                Looks good!
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-12 mb-3">
                            <label for="email">Email</label>
                            <input type="email" class="form-control" id="email" placeholder="Email" required="" autocomplete="off" aria-autocomplete="none">
                            <div class="invalid-feedback">
                                Please provide a valid Email.
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-sm-5 col-10 col-md-5 mb-3">
                            <label for="password">Password</label>
                            <input type="password" class="form-control pass" id="password" placeholder="Password" required="" autocomplete="off">
                            <div class="invalid-feedback">
                                Please provide a valid password.
                            </div>
                        </div>
                        <div class="col-sm-5 col-10 col-md-5 mb-3">
                            <label for="confPassword">Confirm Password</label>
                            <input type="password" class="form-control pass" id="confPassword" placeholder="Password" required="" autocomplete="off">
                            <div class="invalid-feedback">
                                Please provide a valid password.
                            </div>
                        </div>
                        <div class="col-sm-1 col-2 col-md-1 mb-4 d-flex align-self-end justify-content-center">
                            <i id="show-hide" class="icon-eye fs-25"></i>
                        </div>
                    </div>
                    <div class="custom-control custom-checkbox my-2 mr-sm-2">
                        <input type="checkbox" class="custom-control-input" id="agreeTerms" required>
                        <label class="custom-control-label" for="agreeTerms"> Agree to terms and conditions</label>
                        <div class="invalid-feedback">
                            You must agree before submitting.
                        </div>
                    </div>
                    <div class="form-group d-flex bd-highlight mt-4">
                        <a href="#" class="mr-auto p-2 bd-highlight text-black"><i class="icon-arrow-left mr-1"></i>Login instead</a>
                        <button class="btn fashion-bg p-2 px-3 bd-highlight" type="submit">Sign Up</button>
                    </div>
                </form>
                </div>
            </div>
        </div>
    </div>

    <script>
        $("document").ready(function(){
            $(".hoverAni").hover(function(){
                $(this).addClass('animated ' + 'bounceIn');
            });
            $(".hoverAni").bind("animationend webkitAnimationEnd oAnimationEnd MSAnimationEnd",function(){
                $(this).removeClass('animated ' + 'bounceIn');
            });
            $("#show-hide").click(function(){
                var isHide =$(this).hasClass('icon-eye');
                $(this).toggleClass('icon-eye icon-eye-off');
                if (isHide){
                    $('.pass').attr('type','text');
                }else {
                    $('.pass').attr('type','password');
                }
            });
        });
    </script>
</body>
@endsection