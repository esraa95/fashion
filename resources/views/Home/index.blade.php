@extends('layout.header')
@section('style')
        <!-- home css -->
<link href="{{ asset('css/home.css') }}" rel="stylesheet">
    @endsection

@section('content')
<div class="h-100 bg-white position-absolute animated bounceInLeft" style="z-index: -1;left: 4%;width:48%;top:1%">
    <h1 class="homeTitle position-absolute" >WOMEN FASHION CATALOGUE</h1>
</div>
<div class="container" style="margin-top: 5rem">
   <div class="row">
       <div class="col-12 py-4">
           <div class="card-columns mt-2">
               <div class=" tk-space">
               </div>

               <div class="card card-default mb-5 animated pulse">
                   <a  href="{{ route('profile') }}">
                       <div class="portfolio-hover">
                           <div class="d-flex align-items-start flex-column bd-highlight portfolio-hover">
                               <span class="mb-auto p-2 bd-highlight">KAY GARLAND</span>
                               <span class="p-2 bd-highlight d-flex align-self-end" >followers <i class="icon-line d-flex  align-items-center px-1"></i>7k</span>
                               <span class="p-2 bd-highlight d-flex align-self-end">Items <i class="icon-line d-flex  align-items-center px-1"></i>88</span>
                           </div>
                       </div>
                       <img class="img-fluid w-100 h-auto" src="{{ asset('imgs/1.png')}}">
                   </a>
               </div>
               <div class="card card-default mb-5 animated pulse">
                   <a  href="{{ route('profile') }}">
                       <div class="portfolio-hover">
                           <div class="d-flex align-items-start flex-column bd-highlight portfolio-hover">
                               <span class="mb-auto p-2 bd-highlight">DIANA PERTERSEN</span>
                               <span class="p-2 bd-highlight d-flex align-self-end" >followers <i class="icon-line d-flex  align-items-center px-1"></i>7k</span>
                               <span class="p-2 bd-highlight d-flex align-self-end">Items <i class="icon-line d-flex  align-items-center px-1"></i>88</span>
                           </div>
                       </div>
                       <img class="img-fluid w-100 h-auto" src="{{ asset('imgs/10.jpeg')}}">
                   </a>
               </div>
               <div class="card card-default mb-5 animated pulse">
                   <a  href="{{ route('profile') }}">
                       <div class="portfolio-hover">
                           <div class="d-flex align-items-start flex-column bd-highlight portfolio-hover">
                               <span class="mb-auto p-2 bd-highlight">SARA FRAZIER</span>
                               <span class="p-2 bd-highlight d-flex align-self-end" >followers <i class="icon-line d-flex  align-items-center px-1"></i>7k</span>
                               <span class="p-2 bd-highlight d-flex align-self-end">Items <i class="icon-line d-flex  align-items-center px-1"></i>88</span>
                           </div>
                       </div>
                       <img class="img-fluid w-100 h-auto" src="{{ asset('imgs\3.jpg')}}">
                   </a>
               </div>
               <div class="card card-default mb-5 animated pulse">
                   <a  href="{{ route('profile') }}">
                       <div class="portfolio-hover">
                           <div class="d-flex align-items-start flex-column bd-highlight portfolio-hover">
                               <span class="mb-auto p-2 bd-highlight">LYDIA ROBINSON</span>
                               <span class="p-2 bd-highlight d-flex align-self-end" >followers <i class="icon-line d-flex  align-items-center px-1"></i>7k</span>
                               <span class="p-2 bd-highlight d-flex align-self-end">Items <i class="icon-line d-flex  align-items-center px-1"></i>88</span>
                           </div>
                       </div>
                       <img class="img-fluid w-100 h-auto" src="{{ asset('imgs\2.jpeg')}}">
                   </a>
               </div>
               <div class="card card-default mb-5 animated pulse">
                   <a  href="{{ route('profile') }}">
                       <div class="portfolio-hover">
                           <div class="d-flex align-items-start flex-column bd-highlight portfolio-hover">
                               <span class="mb-auto p-2 bd-highlight">CARLA NUNEZ</span>
                               <span class="p-2 bd-highlight d-flex align-self-end" >followers <i class="icon-line d-flex  align-items-center px-1"></i>7k</span>
                               <span class="p-2 bd-highlight d-flex align-self-end">Items <i class="icon-line d-flex  align-items-center px-1"></i>88</span>
                           </div>
                       </div>
                       <img class="img-fluid w-100 h-auto" src="{{ asset('imgs\4.jpg')}}">
                   </a>
               </div>
               <div class=" mb-5 tk-space" style="height: 300px;">
               </div>
               <div class=" mb-5 tk-space">
               </div>
               <div class="card card-default mb-5 animated pulse">
                   <a  href="{{ route('profile') }}">
                       <div class="portfolio-hover">
                           <div class="d-flex align-items-start flex-column bd-highlight portfolio-hover">
                               <span class="mb-auto p-2 bd-highlight">LINA OLIVER</span>
                               <span class="p-2 bd-highlight d-flex align-self-end" >followers <i class="icon-line d-flex  align-items-center px-1"></i>7k</span>
                               <span class="p-2 bd-highlight d-flex align-self-end">Items <i class="icon-line d-flex  align-items-center px-1"></i>88</span>
                           </div>
                       </div>
                       <img class="img-fluid w-100 h-auto" src="{{ asset('imgs\13.jpg')}}">
                   </a>
               </div>
               <div class="card card-default mb-5 animated pulse">
                   <a  href="{{ route('profile') }}">
                       <div class="portfolio-hover">
                           <div class="d-flex align-items-start flex-column bd-highlight portfolio-hover">
                               <span class="mb-auto p-2 bd-highlight">HAZEL WARREN</span>
                               <span class="p-2 bd-highlight d-flex align-self-end" >followers <i class="icon-line d-flex  align-items-center px-1"></i>7k</span>
                               <span class="p-2 bd-highlight d-flex align-self-end">Items <i class="icon-line d-flex  align-items-center px-1"></i>88</span>
                           </div>
                       </div>
                       <img class="img-fluid w-100 h-auto" src="{{ asset('imgs\5.png')}}">
                   </a>
               </div>
               <div class="card card-default mb-2 animated pulse">
                   <a  href="{{ route('profile') }}">
                       <div class="portfolio-hover">
                           <div class="d-flex align-items-start flex-column bd-highlight portfolio-hover">
                               <span class="mb-auto p-2 bd-highlight">BILL GRAVES</span>
                               <span class="p-2 bd-highlight d-flex align-self-end" >followers <i class="icon-line d-flex  align-items-center px-1"></i>7k</span>
                               <span class="p-2 bd-highlight d-flex align-self-end">Items <i class="icon-line d-flex  align-items-center px-1"></i>88</span>
                           </div>
                       </div>
                       <img class="img-fluid w-100 h-auto" src="{{ asset('imgs\9.jpeg')}}">
                   </a>
               </div>
           </div>
       </div>

   </div>
</div>

@endsection